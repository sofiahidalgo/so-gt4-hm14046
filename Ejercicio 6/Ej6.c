#include <stdio.h>
#include <stdlib.h>

void procesos(int n, float *datos){
    float mayor = 0.00, menor = 100.00, media=0.00;
    for (int i = 0; i < n; i++) {
        if(datos[i]>mayor){
            mayor=datos[i];
        }
        if(menor>datos[i]){
            menor=datos[i];
        }
        media=media+datos[i];
    }
    media=media/n;
    printf("\nMayor : %.2f",mayor);
    printf("\nMenor : %.2f",menor);
    printf("\nMedia Aritmetica : %.2f",media);
    printf("\n");
}
int main(){
    int n;
    float ingreso,*datos;
    printf("Ingrese cantidad de numeros a operar: ");
    scanf("%d", &n);
    datos =(float* ) malloc(n *sizeof(int));
    for (int i = 0; i < n; i++){
        printf("Ingrese dato #%d",i+1);
        printf("\t");
        scanf("%f", &ingreso);
        datos[i] = ingreso;
    }
    printf("Datos ingresados: \n");
    for (int i = 0; i < n; i++){
        printf("%.2f", *(datos + i));
	printf("\n");
    }
    printf("\n");
    procesos(n,datos);
    return 0;
}
