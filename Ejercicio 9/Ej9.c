#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main(){
	int cantidad,i,j;

	printf("¿Cuantas palabras desea ordenar?: ");
	scanf("%i",&cantidad);

	char dic[cantidad][50];
	for(i=0; i<cantidad;i++){
		printf("Ingrese la palabra #%i: ",i+1);
		scanf("%49s",dic[i]);
	}

	char tmp[50];
	for(i=0;i<=cantidad;i++){
		for(j=1;j<cantidad;j++){
			if(strcmp(dic[j-1],dic[j]) < 0){
				strcpy(tmp,dic[j]);
				strcpy(dic[j], dic[j-1]);
				strcpy(dic[j-1],tmp);
			}
		}
	}

	for(i=0;i<cantidad;i++){
		printf("%s\n",dic[i]);
	}
}

