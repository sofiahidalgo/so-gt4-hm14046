#include <stdio.h>
#include <stdlib.h>

void ordenar(int n, int *numeros){
    int datos;
    for (int i = 0; i < n; i++) { 
        for (int j = i + 1; j < n; j++) { 
            if(*(numeros + j) < *(numeros + i)){
                datos = *(numeros + i); 
                *(numeros + i) = *(numeros + j); 
                *(numeros + j) = datos; 
            }
        } 
    }
   
    printf("Numeros despues de ordenarlos \n");
    for (int i = 0; i < n; i++) {
        printf(" %d ", *(numeros + i)); 
        printf("\n");
    } 
    printf("\n");       }

int main(){
    int n, *total;
    printf(" \n");	  
    printf("Ingrese cuantos numeros desea ordenar: ");
    scanf("%d", &n);
    printf(" \n");	  

   
    total =(int* ) malloc(n *sizeof(int));

    
    for (int i = 0; i < n; i++){
        total[i] = rand() % 100;
    }

   
    printf("Numeros antes de ser ordenados \n");
    for (int i = 0; i < n; i++){
        printf(" %d ", *(total + i));
       printf("\n");
    }

    printf("\n");
    ordenar(n, total);
    return 0;
}

