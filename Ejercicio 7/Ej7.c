#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void operacion(int n, double *coeficiente){
    double x, res=0.00, datos;
    printf("Ingrese el punto X a evaluar: ");
    printf("\t");
    scanf("%lf", &x);

    for (int i = 0; i < n+1; i++) { 
        datos= pow (x, n-i);
        res=res+(coeficiente[i]*datos);
    }
    printf("\nResultado : %.2lf",res);
    printf("\n");      
}
int main(){
    int n;
    double ingreso,*coeficiente;
    printf("Ingrese grado del polinomio: ");
    scanf("%d", &n);   
    coeficiente =(double* ) malloc(n *sizeof(int)+1);
    for (int i = 0; i < n+1; i++){
        printf("Ingrese coeficiente para X^%d",n-i);
        printf("\t");
        scanf("%lf", &ingreso);
        coeficiente[i] = ingreso;
    }
    printf("Polinomio: \n");
    for (int i = 0; i < n+1; i++){
        printf(" %.2lf", coeficiente[i]);
        printf("X^%d", n-i);
    }

    printf("\n");
    operacion(n,coeficiente);
    return 0;

}
