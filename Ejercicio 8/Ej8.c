#include <stdio.h>
#include <string.h>
char *pedirTexto();
void contarVocales(char *,int[]);
void imprimir(int []);

void main(){

	char *texto;
	int num[5];
	texto = pedirTexto();
	contarVocales(texto, num);
	imprimir(num);
}
char *pedirTexto(){
	static char str[256];
	printf("Ingrese un texto: ");
	fgets(str,256,stdin);
        printf("\n");
	return str;
}

void contarVocales(char *str, int voc[]){
	for(int i=0;i<5;i++){
		voc[i]=0;
	}

	while(*(str) != 0){
		switch(*(str)){
			case 'a':
			case 'A':
				voc[0]++;
			break;
			case 'E':
			case 'e':
				voc[1]++;
			break;
			case 'I':
			case 'i':
				voc[2]++;
			break;
			case 'O':
			case 'o':
				voc[3]++;
			break;
			case 'U':
			case 'u':
				voc[4]++;
			break;
		}
		str++;
	}
}

void imprimir(int voc[]){
	printf("Se encontraron %d  'a'\n",voc[0]);
	printf("Se encontraron %d  'e'\n",voc[1]);
	printf("Se encontraron %d  'i'\n",voc[2]);
	printf("Se encontraron %d  'o'\n",voc[3]);
	printf("Se encontraron %d  'u'\n",voc[4]);
 printf("\n");
	
}
