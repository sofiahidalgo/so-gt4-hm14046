#include <stdio.h>
#include <string.h>

typedef struct {
	char nombre[40];
	int dui;
	float sueldo;
} empleado;

void imprimir(empleado[], int);
void ordenarNombre(empleado[], int);
void ordenarSueldo(empleado[], int);
void promedioSueldo(empleado[], int);

void main(){
	char control;

	int limite = 0;
	double promedio;
	empleado emp[1000];

	while(1){
		printf("Ingrese el nombre del empleado: ");
		scanf("%s",emp[limite].nombre);
		printf("Ingrese el dui del empleado: ");
		scanf("%d",&(emp[limite].dui));
		printf("Ingrese el sueldo del empleado: ");
		scanf("%f",&(emp[limite].sueldo));

		limite++;
		printf("¿Continuar? Y/N ");
		scanf(" %c",&control);

		if(control == 'N' || control == 'n') break;
	}

	printf("\nORDEN POR NOMBRE\n\n");
	ordenarNombre(emp,limite);
	imprimir(emp,limite);

	printf("\nORDEN POR SUELDO\n\n");
	ordenarSueldo(emp,limite);
	imprimir(emp,limite);

	promedioSueldo(emp,limite);
}

void imprimir(empleado emp[],int limite){
	for(int i=0;i<limite;i++){
		printf("Empleado:\n\tNombre: %s\n\tDUI: %d\n\tSueldo: %f\n",
		emp[i].nombre,emp[i].dui,emp[i].sueldo);
	}
}

void ordenarNombre(empleado emp[], int limite){
empleado tmp;

	for(int i=0;i<limite;i++){
		for(int j=1;j<limite;j++){
			if(strcmp(emp[j-1].nombre,emp[j].nombre)>0){
				tmp = emp[j];
				emp[j] = emp[j-1];
				emp[j-1] = tmp;
			}
		}
	}
}

void ordenarSueldo(empleado emp[], int limite){
empleado tmp;

	for(int i=0;i<limite;i++){
		for(int j=1;j<limite;j++){
			if(emp[j].sueldo > emp[j-1].sueldo){
				tmp = emp[j];
				emp[j] = emp[j-1];
				emp[j-1] = tmp;
			}
		}
	}
}

void promedioSueldo(empleado emp[], int limite){
	double res = 0;
	for(int i=0;i<limite;i++){
		res+= emp[i].sueldo;
	}
	printf("El promedio de los sueldos es: %0.2f\n",res/limite);
}

